#Cordova-Network-Toggle
This plugin set / gets the network connection.

Install https://gitlab.com/gogu/cordova-netwsitch.git


com.teralsoft.netswitch


Enable:


@return - String, 'enabled'
window.netswitch.enable({success:dataEnabled});




Disable:


@return - String, 'disabled'
window.netswitch.disable({success:dataDisable});




Toggle:


@return - String, 'enabled/disabled'
window.netswitch.toggle({success:dataToggle});