module.exports = {

    enable:function(args) {
        cordova.exec(
            (!args.success) ? null : args.success, 
            function(error){alert('Mobile Data enable Error:'+error)}, 
            "mobiledata", 
            'enable',
            []
        );
    },
    
    disable:function(args) {
        cordova.exec(
            (!args.success) ? null : args.success, 
            function(error){alert('Mobile Data disable Error:'+error)}, 
            "mobiledata", 
            'disable',
            []
        );
    },
    
    toggle:function(args) {
        cordova.exec(
            (!args.success) ? null : args.success, 
            function(error){alert('Mobile Data toggle Error:'+error)}, 
            "mobiledata", 
            'toggle',
            []
        );
    }     
};